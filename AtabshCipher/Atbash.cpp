#include <string>
#include <iostream>
class Atbash {

	std::string inputText;
	std::string cipher = "abcdefghijklmnopqrstuvwxyz";
	std::string decipher = "zyxwvutsrqponmlkjihgfedcba";

public:
	Atbash(std::string text) {
		inputText = text;
	}
	Atbash() {}

	void SetInputText(std::string text) {
		inputText = text;
	}

	std::string Cipher() {
		std::string outputText;
		for (int i = 0; i < inputText.length(); i++)
		{
			int findId = cipher.find(inputText[i]);
			outputText += decipher[findId];
		}
		return outputText;
	}

	std::string Decipher() {
		std::string outputText;
		for (int i = 0; i < inputText.length(); i++)
		{
			int findId = decipher.find(inputText[i]);
			outputText += cipher[findId];
		}
		return outputText;
	}
};