#include <string>
#include <iostream>
#include <fstream>
#include "Atbash.cpp"

class Menu {

	std::string fileLocation;

public:
	Menu() {};

	void RunCipher() {
		std::string result = CipherRun(CipherMode());
		std::cout << "Result:" << std::endl;
		std::cout << result << std::endl;
	}

	int CipherMode() {
		bool selected = false;
		while (!selected) {
			int selection;
			std::cout << "Select mode" << std::endl;
			std::cout << "1. Cipher" << std::endl;
			std::cout << "2. Decipher" << std::endl;
			std::cin >> selection;
			switch (selection)
			{
			case 1:
				return 1;
				selected = true;
				break;
			case 2:
				return 2;
				selected = true;
				break;
			default:
				std::cout << "Wrong number selected" << std::endl;
				break;
			}
		}
	}

	std::string CipherRun(int cipherMode) {
		std::string word;
		std::string path;
		std::ifstream myFile;
		std::string line;
		Atbash* CiperCore = new Atbash();
		bool selected = false;
		while (!selected) {
			int selection;

			std::cout << "Select input type" << std::endl;
			std::cout << "1. Keyboard" << std::endl;
			std::cout << "2. File" << std::endl;
			std::cin >> selection;
			switch (selection)
			{
			case 1:
				std::cout << "Input your word" << std::endl;
				std::cin >> word;
				CiperCore->SetInputText(word);
				if (cipherMode == 1) {
					return CiperCore->Cipher();
				}
				if (cipherMode == 2) {
					return CiperCore->Decipher();
				}

				selected = true;
				break;

			case 2:
				std::cout << "Input file path" << std::endl;
				std::cin >> path;
				//path = "C:\\test.tx";
				myFile.open(path);
				if (myFile.is_open()) {
					while (getline(myFile, line)) {
						word += line;
					}
					myFile.close();
					CiperCore->SetInputText(word);
					if (cipherMode == 1) {
						return CiperCore->Cipher();
					}
					if (cipherMode == 2) {
						return CiperCore->Decipher();
					}
					selected = true;
					break;
				}

				else {
					std::cout << "Unable to open file" << std::endl;
					selected = false;
					break;
				}

			default:
				std::cout << "Wrong number selected" << std::endl;
				break;
			}

		}
	}
};